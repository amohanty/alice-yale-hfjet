This repository contains the analysis code for the D-meson jet analysis.

The aim of this analysis is to reconstruct jets containing D mesons and study their properties.
The results will be presented as a double differential yield in jet _p_<sub>T</sub> and momentum fraction 
carried by the D meson in the direction of the jet axis (_z_).
As a first step, the analysis will be carried out for a single differential yield in jet _p_<sub>T</sub>.

The analysis progress is documented in a [JIRA ticket] (https://alice.its.cern.ch/jira/browse/PWGHF-108).